import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {MatDialog} from "@angular/material";
import {AuthComponent} from "../../../auth/auth.component";

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  dialogRef;

  constructor(private router: Router,
              private dialog: MatDialog ) { }

  ngOnInit() {
  }

  scroll(target) {
    if (!this.router.isActive('/', true)) {
      this.router.navigateByUrl('/').then(() => {
        document.getElementById(target).scrollIntoView({behavior: 'smooth'});
      });
    } else {
      document.getElementById(target).scrollIntoView({behavior: 'smooth'});
    }
  }

  onLogInClick(authType = 'login') {
    this.dialogRef = this.dialog.open(AuthComponent, {
      data: {authType: authType}
    });

    this.dialogRef.afterClosed().subscribe(response => {
      if (response) {
        this.onLogInClick(response === 'login' ? 'logup' : 'login');
      }
    });
  }

}
