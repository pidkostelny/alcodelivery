import { Component, OnInit } from '@angular/core';



@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  popular = [
    {name: 'Головна', link: '/'},
    {name: 'Каталог', link: '/catalog'},
    {name: 'Віскі', link: '/catalog?category=віскі'},
    {name: 'Горілка', link: '/catalog?category=горілка'},
    {name: 'Пиво', link: '/catalog?category=пиво'},
    {name: 'Текіла', link: '/catalog?category=текіла'},
  ];

  constructor() { }

  ngOnInit() {
  }

}
