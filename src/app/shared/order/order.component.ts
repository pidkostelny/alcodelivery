import {Component, Inject, OnInit} from '@angular/core';
import {CartService} from '../../core';
import {MAT_DIALOG_DATA, MatDialogRef, MatSnackBar} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

const ORDER_DONE_MESSAGE = 'Дякуємо за замовлення, невдовзі з вами зв\'яжуться';
const ORDER_DONE_BUTTON = 'ОК';

const PHONE_REGEXP = /^(((\+|00)?(38))|(38))?0[1-9]{1}\d{8}$/;

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  orderForm: FormGroup;

  constructor(private dialogRef: MatDialogRef<OrderComponent>,
              @Inject(MAT_DIALOG_DATA) private data,
              private snackBar: MatSnackBar,
              private fb: FormBuilder
  ) {
    this.orderForm = this.fb.group({
      name: ['', [Validators.required] ],
      surname: ['', [Validators.required] ],
      phoneNumber: ['', [Validators.pattern(PHONE_REGEXP), Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      address: ['', [Validators.required]],
      wish: [''],
    });
  }

  ngOnInit() {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onOrderClick() {
    this.dialogRef.close();
    this.snackBar.open(ORDER_DONE_MESSAGE, ORDER_DONE_BUTTON);
  }

  get name() {
    return this.orderForm.get('name');
  }

  get surname() {
    return this.orderForm.get('surname');
  }

  get phoneNumber() {
    return this.orderForm.get('phoneNumber');
  }

  get email() {
    return this.orderForm.get('email');
  }

  get address() {
    return this.orderForm.get('address');
  }
}

