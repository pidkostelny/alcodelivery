

import {MatCheckboxModule, MatIconModule, MatButtonModule, MatRadioModule, MatSliderModule, MatInputModule, MatBadgeModule, MatListModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatTooltipModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {OrderComponent} from './order/order.component';
import {CartComponent} from './cart/cart.component';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    MatIconModule, MatInputModule, MatButtonModule, MatRadioModule, MatSliderModule, MatCheckboxModule, MatExpansionModule, MatBadgeModule, MatListModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatTooltipModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  exports: [
    MatIconModule, MatInputModule, MatButtonModule, MatRadioModule, MatSliderModule, MatCheckboxModule, MatExpansionModule, MatBadgeModule, MatListModule, MatDialogModule, MatSnackBarModule, MatSelectModule, MatTooltipModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SharedModule {}
