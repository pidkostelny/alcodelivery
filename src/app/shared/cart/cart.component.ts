import { Component, OnInit } from '@angular/core';
import { CartService, CartProduct } from '../../core';
import {MatDialog} from '@angular/material';
import {OrderComponent} from '../order/order.component';

const OFFSET_HEIGHT = 141;
const PRODUCT_HEIGHT = 91;

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  productAdded = false;
  products: CartProduct[] = [];
  cartTotal = 0;
  numProducts = 0;
  expanded = false;
  expandedHeight: string;

  constructor(private cartService: CartService,
              private dialog: MatDialog) { }

  ngOnInit() {
    this.cartService.productAdded$.subscribe(data => {
      this.products = data.products;
      this.cartTotal = data.cartTotal;
      this.numProducts = data.numProducts;

      this.expandedHeight = this.products.length * PRODUCT_HEIGHT + OFFSET_HEIGHT + 'px';

      this.productAdded = true;
      setTimeout(() => {
        this.productAdded = false;
      }, 400);

      if (!this.products.length) {
        this.expanded = false;
      }
    });
  }

  onCartClick() {
    this.expanded = !this.expanded;
  }

  countUp(product: CartProduct) {
    this.cartService.countUp(product);
  }

  countDown(product: CartProduct) {
    this.cartService.countDown(product);
  }

  deleteProduct(product: CartProduct) {
    this.cartService.deleteProductFromCart(product);
  }

  makeOrder() {
    const dialogRef = this.dialog.open(OrderComponent, {
      maxWidth: '320px',
      data: {cartTotal: this.cartTotal}
    });
  }
}
