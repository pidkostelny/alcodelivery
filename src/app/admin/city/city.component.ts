import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog, Sort, PageEvent} from '@angular/material';
import {City, Pagination} from '../../core/models';
import {CreateCityComponent} from './create-city.component';
import {Subscription} from 'rxjs';
import {CityService} from '../../core/services/city.service';

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {
  displayedColumns: string[] = ['id', 'name', 'country', 'actions'];
  dataSource: MatTableDataSource<City>;

  data$: Subscription;
  filterValue = '';
  totalItems = 0;
  pageOptions = new Pagination();

  constructor(private cityService: CityService,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateCityComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.cityService.update(response).subscribe(city => {
          this.updateData();
        });
      } else if (response) {
        this.cityService.create(response).subscribe(city => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    this.cityService.delete(id).subscribe(() => {
      this.updateData();
    });
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      this.data$ = this.cityService.findOneByName(search).subscribe(data => {
        this.showDataFromApi(data);
      });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.cityService.getPage(this.pageOptions).subscribe(data => {
      this.showDataFromApi(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private showDataFromApi(data: any) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.pagination.total;
  }

}
