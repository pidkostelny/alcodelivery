import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import {SharedModule} from '../shared/shared.module';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from './admin.component';
import {CountryComponent} from './country/country.component';
import {CityComponent} from './city/city.component';
import {MatPaginatorModule, MatSortModule, MatTableModule, MatSlideToggleModule, MatAutocompleteModule} from '@angular/material';
import {CreateCountryComponent} from './country/create-country.component';
import {CreateCityComponent} from './city/create-city.component';
import {TypeComponent} from './type/type.component';
import {CreateTypeComponent} from './type/create-type.component';
import {SubtypeComponent} from './subtype/subtype.component';
import {CreateSubtypeComponent} from './subtype/create-subtype.component';
import {CommodityComponent} from './commodity/commodity.component';
import {CreateCommodityComponent} from './commodity/create-commodity.component';
import {FirmComponent} from './firm/frim.component';
import {CreateFirmComponent} from './firm/create-firm.component';

@NgModule({
  imports: [
    AdminRoutingModule,
    SharedModule,
    MatPaginatorModule, MatSortModule, MatTableModule, MatSlideToggleModule, MatAutocompleteModule
  ],
  declarations: [
    AdminComponent,
    CountryComponent,
    CreateCountryComponent,
    CityComponent,
    CreateCityComponent,
    TypeComponent,
    CreateTypeComponent,
    SubtypeComponent,
    CreateSubtypeComponent,
    CommodityComponent,
    CreateCommodityComponent,
    FirmComponent,
    CreateFirmComponent
  ],
  entryComponents: [
    CreateCountryComponent,
    CreateCityComponent,
    CreateTypeComponent,
    CreateSubtypeComponent,
    CreateCommodityComponent,
    CreateFirmComponent
  ]
})
export class AdminModule {}
