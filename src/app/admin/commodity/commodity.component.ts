import { Component, OnInit } from '@angular/core';
import {MatDialog, MatTableDataSource, PageEvent, Sort} from '@angular/material';
import {CommoditySearch, Country, Pagination} from '../../core/models';
import {Subscription} from 'rxjs';
import {CommodityService} from '../../core/services';
import {CreateCommodityComponent} from './create-commodity.component';

@Component({
  selector: 'app-commodity',
  templateUrl: './commodity.component.html',
  styleUrls: ['./commodity.component.scss']
})
export class CommodityComponent implements OnInit {
  noImageFilePath = '../../../assets/no-image.png';

  displayedColumns: string[] = ['image', 'id', 'name', 'firm', 'subtype', 'price', 'capacity', 'description', 'strength', 'maturity', 'actions'];
  dataSource: MatTableDataSource<any>;

  data$: Subscription;
  filterValue = '';
  pageOptions = new Pagination();

  constructor(private commodityService: CommodityService,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateCommodityComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.commodityService.update(response).subscribe(commodity => {
          this.updateData();
        });
      } else if (response) {
        this.commodityService.create(response).subscribe(commodity => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    this.commodityService.delete(id).subscribe(() => {
      this.updateData();
    });
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      const searhRequest = new CommoditySearch(this.pageOptions, `%${search}%`);
      this.data$ = this.commodityService.search(searhRequest).subscribe(data => {
        this.showDataFromApi(data);
      });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.commodityService.getPage(this.pageOptions).subscribe(data => {
      this.showDataFromApi(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private showDataFromApi(data: any) {
    this.dataSource = new MatTableDataSource(data.content);
    this.pageOptions.total = data.pagination.total;
  }

}
