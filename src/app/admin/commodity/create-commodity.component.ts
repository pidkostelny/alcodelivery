
import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {FirmService, SubtypeService, TypeService} from '../../core/services';
import {Country, Firm, Subtype, Type} from '../../core/models';

@Component({
  selector: 'app-create-commodity',
  templateUrl: './create-commodity.component.html',
  styleUrls: ['./create-commodity.component.scss']
})
export class CreateCommodityComponent implements OnInit {

  createForm: FormGroup;
  showMaturity = false;
  types: Type[] = [];
  subtypes: Subtype[] = [];
  firms: Firm[] = [];
  imgSource = '/assets/no-image.svg';

  constructor(private typeService: TypeService,
              private subtypeService: SubtypeService,
              private firmService: FirmService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateCommodityComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {
    const el: any = this.data.element || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required]],
      firm: ['', [Validators.required]],
      type: [el.typeName, [Validators.required]],
      subtype: ['', [Validators.required]],
      capacity: [el.capacity, [Validators.required]],
      description: [el.description, [Validators.required]],
      strength: [el.strength, [Validators.required]],
      maturity: [el.maturity, [Validators.required]],
      price: [el.price, [Validators.required]]
    });

    if (this.data.element) {
      this.initValuesForUpdate(el);
    }
  }

  ngOnInit() {
    this.typeService.getAll().subscribe(response => {
      this.types = response.content;
    });
  }

  onCreateEntity() {
    const response = this.createForm.invalid ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      subTypeId: this.subtype.value.id,
      firmId: this.firm.value.id,
      capacity: this.capacity.value,
      description: this.description.value,
      strength: this.strength.value,
      maturity: this.maturity.value,
      price: this.price.value,
      image: this.imgSource,
    };
    this.dialogRef.close(response);
  }

  typeChange() {
    this.typeService.findOneByName(this.type.value).subscribe(response => {
      this.types = response.content;
    });
  }

  loadSubtypes(id) {
    this.typeChange();
    this.subtypes = [];
    this.subtypeService.getAllByTypeId(id).subscribe(response => {
      this.subtypes = response.content;
      if (!this.subtypes.length) {
        alert('В даного типу немає підтипів');
      }
    });
  }

  firmChange() {
    this.firmService.findOneByName(this.firm.value).subscribe(response => {
      this.firms = response.content;
    });
  }

  photoSelected(target) {
    const file: File = target.files[0];
    const reader: FileReader = new FileReader();

    reader.onload = (e) => {
      this.imgSource = reader.result;
    };
    reader.readAsDataURL(file);
  }

  private initValuesForUpdate(el: any) {
    this.imgSource = el.pathToImage;
    this.firmService.findOneByName(el.firmName).subscribe(firms => {
      this.firm.setValue(firms.content[0]);
    });
    this.subtypeService.findOneByName(el.subTypeName).subscribe(subtypes => {
      this.subtype.setValue(subtypes.content[0]);
      this.typeService.findOneByName(this.subtype.value.typeName).subscribe(types => {
        this.type.setValue(types.content[0]);
      });
    });
  }

  private autocompleateDisplay(value) {
    return value ? value.name : undefined;
  }

  get name() {
    return this.createForm.get('name');
  }

  get firm() {
    return this.createForm.get('firm');
  }

  get type() {
    return this.createForm.get('type');
  }

  get subtype() {
    return this.createForm.get('subtype');
  }

  get capacity() {
    return this.createForm.get('capacity');
  }

  get description() {
    return this.createForm.get('description');
  }

  get strength() {
    return this.createForm.get('strength');
  }

  get maturity() {
    return this.createForm.get('maturity');
  }

  get price() {
    return this.createForm.get('price');
  }

}
