import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, PageEvent, Sort} from '@angular/material';
import {Country, Pagination} from '../../core/models';
import {CreateCountryComponent} from './create-country.component';
import {Subscription} from 'rxjs';
import {CountryService} from '../../core/services';

@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.scss']
})
export class CountryComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'actions'];
  dataSource: MatTableDataSource<Country>;

  data$: Subscription;
  filterValue = '';
  totalItems = 0;
  pageOptions = new Pagination();

  constructor(private countryService: CountryService,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateCountryComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.countryService.update(response).subscribe(country => {
          this.updateData();
        });
      } else if (response) {
        this.countryService.create(response).subscribe(country => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    this.countryService.delete(id).subscribe(() => {
      this.updateData();
    });
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      this.data$ = this.countryService.findOneByName(search).subscribe(data => {
        this.showDataFromApi(data);
      });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.countryService.getPage(this.pageOptions).subscribe(data => {
      this.showDataFromApi(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private showDataFromApi(data: any) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.pagination.total;
  }
}
