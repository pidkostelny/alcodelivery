import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, PageEvent, Sort} from '@angular/material';
import {Pagination, Firm} from '../../core/models';
import {CreateFirmComponent} from './create-firm.component';
import {Subscription} from 'rxjs';
import {FirmService} from '../../core/services';

@Component({
  selector: 'app-firm',
  templateUrl: './firm.component.html',
  styleUrls: ['./firm.component.scss']
})
export class FirmComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'countryName', 'actions'];
  dataSource: MatTableDataSource<Firm>;

  data$: Subscription;
  filterValue = '';
  totalItems = 0;
  pageOptions = new Pagination();

  constructor(private firmService: FirmService,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateFirmComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.firmService.update(response).subscribe(firm => {
          this.updateData();
        });
      } else if (response) {
        this.firmService.create(response).subscribe(firm => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    this.firmService.delete(id).subscribe(() => {
      this.updateData();
    });
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      this.data$ = this.firmService.findOneByName(search).subscribe(data => {
        this.showDataFromApi(data);
      });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.firmService.getPage(this.pageOptions).subscribe(data => {
      this.showDataFromApi(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private showDataFromApi(data: any) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.pagination.total;
  }
}
