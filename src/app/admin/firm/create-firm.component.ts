import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CountryService} from '../../core/services';
import {Country} from '../../core/models';

@Component({
  selector: 'app-create-firm',
  templateUrl: './create-firm.component.html',

})
export class CreateFirmComponent implements OnInit {

  createForm: FormGroup;
  countries: Country[] = [];

  constructor(private countryService: CountryService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateFirmComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {}

  ngOnInit() {
    const el: any = this.data.element || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required]],
      country: ['', [Validators.required]]
    });
    this.initValuesForUpdate(el);

    this.countryService.getPage().subscribe(response => {
      this.countries = response.content;
    });
  }

  typeChange() {
    this.countryService.findOneByName(this.country.value).subscribe(response => {
      this.countries = response.content;
    });
  }

  onCreateEntity() {
    const response = this.createForm.invalid ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      countryId: this.country.value.id
    };
    this.dialogRef.close(response);
  }

  initValuesForUpdate(el: any) {
    this.countryService.findOneByName(el.countryResponse.name).subscribe(countries => {
      el.country = countries.content[0];
      this.country.setValue(el.country);
    });
  }

  autocompleateDisplay(country?: Country) {
    return country ? country.name : undefined;
  }

  get name() {
    return this.createForm.get('name');
  }

  get country() {
    return this.createForm.get('country');
  }

}
