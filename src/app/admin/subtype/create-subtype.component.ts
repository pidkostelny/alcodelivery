import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TypeService} from '../../core/services';
import {Type} from '../../core/models';

@Component({
  selector: 'app-create-type',
  templateUrl: './create-subtype.component.html',

})
export class CreateSubtypeComponent implements OnInit {

  createForm: FormGroup;
  types: Type[] = [];

  constructor(private typeService: TypeService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateSubtypeComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {}

  ngOnInit() {
    const el: any = this.data.element || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required]],
      type: ['', [Validators.required]]
    });
    this.initValuesForUpdate(el);

    this.typeService.getAll().subscribe(response => {
      this.types = response.content;
    });
  }

  typeChange() {
    this.typeService.findOneByName(this.type.value).subscribe(response => {
      this.types = response.content;
    });
  }

  onCreateEntity() {
    const response = this.createForm.invalid ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      typeId: this.type.value.id
    };
    this.dialogRef.close(response);
  }

  initValuesForUpdate(el: any) {
    this.typeService.findOneByName(el.typeName).subscribe(types => {
      el.type = types.content[0];
      this.type.setValue(el.type);
    });
  }

  autocompleateDisplay(type?: Type) {
    return type ? type.name : undefined;
  }

  get name() {
    return this.createForm.get('name');
  }

  get type() {
    return this.createForm.get('type');
  }

}
