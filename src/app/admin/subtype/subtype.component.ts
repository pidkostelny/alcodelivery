import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource, MatDialog, PageEvent, Sort} from '@angular/material';
import {Pagination, Subtype} from '../../core/models';
import {CreateSubtypeComponent} from './create-subtype.component';
import {Subscription} from 'rxjs';
import {SubtypeService} from '../../core/services';

@Component({
  selector: 'app-subtype',
  templateUrl: './subtype.component.html',
  styleUrls: ['./subtype.component.scss']
})
export class SubtypeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'typeName', 'actions'];
  dataSource: MatTableDataSource<Subtype>;

  data$: Subscription;

  filterValue = '';
  totalItems = 0;
  pageOptions = new Pagination();

  constructor(private subtypeService: SubtypeService,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateSubtypeComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.subtypeService.update(response).subscribe(subType => {
          this.updateData();
        });
      } else if (response) {
        this.subtypeService.create(response).subscribe(subType => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
    this.subtypeService.delete(id).subscribe(() => {
      this.updateData();
    });
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      this.data$ = this.subtypeService.findOneByName(`%${search}%`).subscribe(data => {
        this.showDataFromApi(data);
      });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.subtypeService.getPage(this.pageOptions).subscribe(data => {
      this.showDataFromApi(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private showDataFromApi(data: any) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.pagination.total;
  }
}
