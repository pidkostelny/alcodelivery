import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './admin.component';
import {CountryComponent} from "./country/country.component";
import {CityComponent} from "./city/city.component";
import {TypeComponent} from "./type/type.component";
import {SubtypeComponent} from "./subtype/subtype.component";
import {CommodityComponent} from "./commodity/commodity.component";
import {FirmComponent} from './firm/frim.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'country',
        component: CountryComponent
      },
      {
        path: 'city',
        component: CityComponent
      },
      {
        path: 'type',
        component: TypeComponent
      },
      {
        path: 'subtype',
        component: SubtypeComponent
      },
      {
        path: 'commodity',
        component: CommodityComponent
      },
      {
        path: 'firm',
        component: FirmComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}
