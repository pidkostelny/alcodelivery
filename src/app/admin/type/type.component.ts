import {Component, OnInit} from '@angular/core';
import {MatTableDataSource, MatDialog, PageEvent, Sort} from '@angular/material';
import {Pagination, Type} from '../../core/models';
import {CreateTypeComponent} from './create-type.component';
import {TypeService} from '../../core/services';
import {Observable, Subscription} from 'rxjs';

@Component({
  selector: 'app-type',
  templateUrl: './type.component.html',
  styleUrls: ['./type.component.scss']
})
export class TypeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'maturity', 'actions'];
  dataSource: MatTableDataSource<Type>;

  data$: Subscription;

  filterValue = '';
  totalItems = 0;
  pageOptions = new Pagination();

  constructor(private typeService: TypeService,
              private dialog: MatDialog) {}

  ngOnInit() {
    this.updateData();
  }

  createElement(element) {
    const dialogRef = this.dialog.open(CreateTypeComponent, {
      data: {element: element}
    });

    dialogRef.afterClosed().subscribe(response => {
      if (response && response.id) {
        this.typeService.update(response).subscribe(type => {
          this.updateData();
        });
      } else if (response) {
        this.typeService.create(response).subscribe(type => {
          this.updateData();
        });
      }
    });
  }

  removeItem(id: number) {
      this.typeService.delete(id).subscribe(() => {
        this.updateData();
      });
  }

  sortDataChange(event: Sort) {
    this.pageOptions.sort.direction = event.direction.toUpperCase() || 'ASC';
    this.pageOptions.sort.fieldName = event.active || 'name';
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  paginationChange(event: PageEvent) {
    this.pageOptions.page = event.pageIndex;
    this.pageOptions.size = event.pageSize;
    this.filterValue.trim() ? this.applyFilter() : this.updateData();
  }

  applyFilter() {
    const search = this.filterValue.trim().toLowerCase();
    if (search) {
      this.pageOptions.page = 0;
      this.resetData();

      this.data$ = this.typeService.findOneByName(search).subscribe(data => {
        this.showDataFromApi(data);
      });
    } else {
      this.updateData();
    }
  }

  private updateData() {
    this.resetData();
    this.data$ = this.typeService.getPage(this.pageOptions).subscribe(data => {
      this.showDataFromApi(data);
    });
  }

  private resetData() {
    if (this.data$) this.data$.unsubscribe();
    this.dataSource = null;
  }

  private showDataFromApi(data: any) {
    this.dataSource = new MatTableDataSource(data.content);
    this.totalItems = data.pagination.total;
  }
}
