import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-create-type',
  templateUrl: './create-type.component.html',

})
export class CreateTypeComponent implements OnInit {

  createForm: FormGroup;


  constructor(private fb: FormBuilder,
              private dialogRef: MatDialogRef<CreateTypeComponent>,
              @Inject(MAT_DIALOG_DATA) private data) {
  }

  ngOnInit() {
    const el: any = this.data.element || {};
    this.createForm = this.fb.group({
      name: [el.name, [Validators.required] ],
      maturity: false
    });
  }

  onCreateEntity() {
    const response = this.createForm.invalid ? null : {
      id: this.data.element && this.data.element.id,
      name: this.name.value,
      maturity: this.maturity.value
    };
    this.dialogRef.close(response);
  }

  get name() {
    return this.createForm.get('name');
  }

  get maturity() {
    return this.createForm.get('maturity');
  }

}
