import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {UserService} from '../core/services/user.service';

const PHONE_REGEXP = /^(((\+|00)?(38))|(38))?0[1-9]{1}\d{8}$/;

const LOGIN_TITLE = 'Вхід в профіль';
const LOGUP_TITLE = 'Реєстрація';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  authType: String = 'login';
  title: String;
  authForm: FormGroup;

  constructor(private userService: UserService,
              private fb: FormBuilder,
              private dialogRef: MatDialogRef<AuthComponent>,
              @Inject(MAT_DIALOG_DATA) private data
  ) {
    this.authForm = this.fb.group({
      name: ['', [Validators.required]],
      surname: ['', [Validators.required] ],
      phoneNumber: ['', [Validators.pattern(PHONE_REGEXP), Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      passwordRepeat: ['', [Validators.required]]
    });
  }

  ngOnInit() {
    this.authType = this.data.authType;
    this.title = this.authType === 'login' ? LOGIN_TITLE : LOGUP_TITLE;
  }

  changeAuthType() {
    this.dialogRef.close(this.authType);
    // if (this.authType === 'login') {
    //   this.authType = 'logup';
    //   this.title = LOGUP_TITLE;
    // } else {
    //   this.authType = 'login';
    //   this.title = LOGIN_TITLE;
    // }
  }

  submitForm() {

  }

  get name() {
    return this.authForm.get('name');
  }

  get surname() {
    return this.authForm.get('surname');
  }

  get phoneNumber() {
    return this.authForm.get('phoneNumber');
  }

  get email() {
    return this.authForm.get('email');
  }

  get password() {
    return this.authForm.get('password');
  }

  get passwordRepeat() {
    return this.authForm.get('passwordRepeat');
  }

}
