import { ModuleWithProviders, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HomeComponent } from './home.component';

import { HomeRoutingModule } from './home-routing.module';
import {SharedModule} from "../shared/shared.module";
import {CarouselComponent} from "./carousel/carousel.component";

@NgModule({
  imports: [
    HomeRoutingModule,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    CarouselComponent
  ]
})
export class HomeModule {}
