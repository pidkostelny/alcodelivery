import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  proposals = [
    {img: 'img-1.png', title: 'Martini', price: 300, volume: 750},
    {img: 'img-2.png', title: 'Jameson', price: 450, volume: 700},
    {img: 'img-3.png', title: 'Finlandia', price: 220, volume: 500},
    {img: 'img-4.png', title: 'Martini', price: 300, volume: 750},
    {img: 'img-5.png', title: 'Jameson', price: 450, volume: 700},
    {img: 'img-6.png', title: 'Finlandia', price: 220, volume: 500},
    {img: 'img-1.png', title: 'Martini', price: 300, volume: 750},
  ];

  constructor(private router: Router) { }

  ngOnInit() {

  }

  navigateToCatalog() {
    this.router.navigateByUrl('/catalog').then(() => {
      scrollTo(0, 0);
    });
  }
}
