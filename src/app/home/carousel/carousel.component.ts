import {AfterViewInit, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit, AfterViewInit {


  @Input() elements;

  centerElement = 0;

  arrowAffected = false;
  arrowAffectInterval;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initCaroseul();
  }

  private initCaroseul() {
    let elements: any = document.getElementsByClassName('proposal-item');
    let showed = [];
    for (let next = this.centerElement, j = 3; j < 7; j++) {
      showed[j] = next;
      next < elements.length - 1 ? next++ : next = 0;
    }
    for (let prev = this.centerElement, j = 2; j >= 0; j--) {
      prev > 0 ? prev-- : prev =  elements.length - 1;
      showed[j] = prev;
    }

    elements[showed[0]].style.zIndex = '1';
    elements[showed[6]].style.zIndex = '1';
    for (let i = 1; i < showed.length - 1; i++) {
      elements[showed[i]].style.left = (i  * 20 - 20) + '%';
      elements[showed[i]].style.zIndex = '2';
      elements[showed[i]].children[0].style.display = 'flex';
    }

    elements[showed[1]].style.transform = 'scale(0.7)';
    elements[showed[5]].style.transform = 'scale(0.7)';
    elements[showed[2]].style.transform = 'scale(0.85)';
    elements[showed[4]].style.transform = 'scale(0.85)';
    elements[showed[3]].style.transform = 'scale(1)';

    elements[showed[0]].children[0].style.display = 'none';
    elements[showed[6]].children[0].style.display = 'none';
    elements[showed[0]].style.transform = 'scale(0.5)';
    elements[showed[6]].style.transform = 'scale(0.5)';
    elements[showed[0]].style.left = '0%';
    elements[showed[6]].style.left = '80%';
  }

  moveSlider(direction) {
    if (!this.arrowAffected) {
      clearInterval(this.arrowAffectInterval);
      this.arrowAffected = true;

      if (direction === 'left') {
        this.centerElement < this.elements.length - 1 ? this.centerElement++ : this.centerElement = 0;
      } else {
        this.centerElement > 0 ? this.centerElement-- : this.centerElement = this.elements.length - 1;
      }

      this.initCaroseul();

      let self = this;
      this.arrowAffectInterval = setTimeout(function () {
        self.arrowAffected = false;
      }, 400);
    }
  }

}
