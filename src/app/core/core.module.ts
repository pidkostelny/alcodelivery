import {NgModule} from '@angular/core';

import {
  ApiService,
  CartService,
  JwtService,
  UserService,
  CommodityService,
  TypeService,
  SubtypeService,
  CountryService,
  FirmService,
  CityService,
} from './services';


@NgModule({
  providers: [
    ApiService,
    CartService,
    JwtService,
    UserService,
    TypeService,
    SubtypeService,
    CountryService,
    FirmService,
    CommodityService,
    CityService
  ]
})
export class CoreModule {}
