export interface Type {
  id: number;
  name: string;
  maturity: boolean;
}
