
export interface Alcoholic {
  email: string;
  name: string;
  surname: string;
  password: string;
  phone_number: string;
  registered: boolean;
  role: number;
  token: string;
}
