import {Pagination} from './pagination.model';

export class CommoditySearch {
  constructor(public paginationRequest: Pagination = new Pagination(0, 12),
              public searchName = '',
              public minPrice = 0,
              public maxPrice = 100000,
              public minVolume = 0.0,
              public maxVolume = 3000,
              public firmsId: number[] = [],
              public countriesId: number[] = []
              ) {}
}
