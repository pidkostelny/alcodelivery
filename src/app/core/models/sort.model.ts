
export class Sort {
  constructor(public direction: string = 'ASC',
              public fieldName: string = 'name'
  ) {}
}
