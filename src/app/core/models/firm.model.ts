import {Country} from './country.model';

export interface Firm {
  id: number;
  name: string;
  countryResponse: Country;
}
