export interface Subtype {
  id: number;
  name: string;
  typeName: string;
}
