import {Sort} from './sort.model';

export class Pagination {
  constructor(public page: number = 0,
              public size: number = 10,
              public sort: Sort = new Sort(),
              public total: number = 0
  ) {}
}

