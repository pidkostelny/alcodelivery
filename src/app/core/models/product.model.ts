
export interface Product {
  id: number;
  capacity: number;
  description: string;
  firm: string;
  subtype: string;
  maturity: number;
  name: string;
  strength: number;
  price: number;
  img: string;
}
