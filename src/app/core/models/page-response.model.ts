
export interface PageResponse {
  content: any[];
  pagination: {
    page: number,
    pageSize: number,
    total: number,
  };
}
