import {Sort} from './sort.model';

export class CommodityFilter {
  public static MIN_PRICE = 0;
  public static MAX_PRICE = 4000;
  public static MIN_VOLUME = 0;
  public static MAX_VOLUME = 4;

  constructor(public minPrice = CommodityFilter.MIN_PRICE,
              public maxPrice = CommodityFilter.MAX_PRICE,
              public minVolume = CommodityFilter.MIN_VOLUME,
              public maxVolume = CommodityFilter.MAX_VOLUME,
              public search = '',
              public sort = new Sort()) {}
}
