import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {PageResponse, Pagination, Type} from '../models';
import {Observable, pipe} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class FirmService {

  public static URL = '/firm';

  constructor(private api: ApiService) {}

  getAll(): Observable<PageResponse> {
    return this.api.get(`${FirmService.URL}`);
  }

  getPage(pagination: Pagination): Observable<PageResponse> {
    return this.api.post(`${FirmService.URL}/page`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(FirmService.URL, new HttpParams().set('id', id.toString()));
  }

  create(type: any) {
    return this.api.post(FirmService.URL, type);
  }

  update(type: Type) {
    return this.api.put(FirmService.URL, type, new HttpParams().set('id', type.id.toString()));
  }

  findOneByName(name: string) {
    return this.api.get(`${FirmService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  }
}
