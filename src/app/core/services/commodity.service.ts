import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {CommoditySearch, PageResponse, Pagination, Type} from '../models';
import {Observable, pipe} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class CommodityService {

  public static URL = '/goods';

  constructor(private api: ApiService) {}

  getAll(): Observable<PageResponse> {
    return this.api.get(CommodityService.URL);
  }

  getPage(pagination: Pagination): Observable<PageResponse> {
    return this.api.post(`${CommodityService.URL}/page`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(CommodityService.URL, new HttpParams().set('id', id.toString()));
  }

  create(type: any) {
    return this.api.post(CommodityService.URL, type);
  }

  update(type: Type) {
    return this.api.put(CommodityService.URL, type, new HttpParams().set('id', type.id.toString()));
  }

  search(search: CommoditySearch) {
    return this.api.post(`${CommodityService.URL}/search`, search);
  }
}
