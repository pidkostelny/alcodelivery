import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {PageResponse, Pagination, Type} from '../models';
import {Observable, pipe} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class TypeService {

  public static URL = '/type';

  constructor(private api: ApiService) {}

  getAll(): Observable<PageResponse> {
    return this.api.get(`${TypeService.URL}/all`);
  }

  getPage(pagination: Pagination): Observable<PageResponse> {
    return this.api.post(`${TypeService.URL}/page`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(TypeService.URL, new HttpParams().set('id', id.toString()));
  }

  create(type: any) {
    return this.api.post(TypeService.URL, type);
  }

  update(type: Type) {
    return this.api.put(TypeService.URL, type, new HttpParams().set('id', type.id.toString()));
  }

  findOneByName(name: string) {
    return this.api.get(`${TypeService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  }
}
