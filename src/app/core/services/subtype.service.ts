import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {PageResponse, Pagination, Subtype} from '../models';
import {Observable, pipe} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class SubtypeService {

  public static URL = '/subType';

  constructor(private api: ApiService) {

  }

  getPage(pagination: Pagination): Observable<PageResponse> {
    return this.api.post(`${SubtypeService.URL}/page`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(SubtypeService.URL, new HttpParams().set('id', id.toString()));
  }

  create(subtype: Subtype) {
    return this.api.post(SubtypeService.URL, subtype);
  }

  update(subtype: Subtype) {
    return this.api.put(SubtypeService.URL, subtype, new HttpParams().set('id', subtype.id.toString()));
  }

  getAllByTypeId(id) {
    return this.api.get(`${SubtypeService.URL}/by/idType`, new HttpParams().set('idType', id));
  }

  findOneByName(name: string) {
    return this.api.get(`${SubtypeService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  }
}
