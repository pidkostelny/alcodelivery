import { Injectable } from '@angular/core';
import {Subject} from 'rxjs/index';
import {CartProduct, Product} from '../models';


@Injectable()
export class CartService {

  products: CartProduct[] = [];
  cartTotal = 0;
  numProducts = 0;

  private productAddedSource = new Subject<any>();


  productAdded$ = this.productAddedSource.asObservable();

  constructor() { }

  addProductToCart(product: Product) {
    let exists = false;
    this.cartTotal += product.price;

    this.products = this.products.map(_product => {
      if (_product.product.id === product.id) {
        _product.count++;
        exists = true;
      }
      return _product;
    });

    if (!exists) {
      this.products.push({
        product: product,
        count: 1
      });
    }

    this.numProducts++;

    this.nextValue();
  }

  deleteProductFromCart(product: CartProduct) {
    this.products = this.products.filter(_product => {
      if (_product.product.id === product.product.id) {
        this.cartTotal -= _product.product.price * _product.count;
        this.numProducts -= _product.count;
        return false;
      }
      return true;
     });
    this.nextValue();
  }

  countUp(product: CartProduct) {
    product.count++;
    this.numProducts++;
    this.cartTotal += product.product.price;
    this.nextValue();
  }

  countDown(product: CartProduct) {
    if (product.count === 1) {
      this.deleteProductFromCart(product);
    } else {
      product.count--;
      this.numProducts--;
      this.cartTotal -= product.product.price;
    }
    this.nextValue();
  }

  flushCart() {
    this.products = [];
    this.cartTotal = 0;
    this.numProducts = 0;
    this.nextValue();
  }

  private nextValue() {
    this.productAddedSource.next({ products: this.products, cartTotal: this.cartTotal, numProducts: this.numProducts });
  }
}
