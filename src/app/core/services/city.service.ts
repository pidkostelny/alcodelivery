import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {City, PageResponse, Pagination} from '../models';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';


@Injectable()
export class CityService {
  public static URL = '/city';

  constructor(private api: ApiService) {

  }

  getPage(pagination: Pagination): Observable<PageResponse> {
    return this.api.post(`${CityService.URL}/page`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(CityService.URL, new HttpParams().set('id', id.toString()));
  }

  create(city: City) {
    return this.api.post(CityService.URL, city);
  }

  update(city: City) {
    return this.api.put(CityService.URL, city, new HttpParams().set('id', city.id.toString()));
  }

  getAllByCountryId(id) {
    return this.api.get(`${CityService.URL}/by/idCountry`, new HttpParams().set('idCountry', id));
  }

  findOneByName(name: string) {
    return this.api.get(`${CityService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  }
}
