import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';

import { throwError, Observable } from 'rxjs';
import { JwtService } from './jwt.service';
import {catchError} from 'rxjs/internal/operators';


@Injectable()
export class ApiService {

  headers: HttpHeaders;

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  private formatErrors(error: any) {
    console.log(error)
    if (error.status === 500 && error.error.message.match(/could not execute statement|You can not delete/)) {
      alert('Не можна видалити, оскільки до цєї одиниці прив\'язані інші');
    }
    return throwError(error.error);
  }

  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    const headers = new HttpHeaders().set('Access-Control-Allow-Origin', '*');
    return this.http.get(`${environment.api_url}${path}`, {
      params: params,
      headers: headers
    }).pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}, params: HttpParams = new HttpParams()): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});

    return this.http.put(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      {
        headers: headers,
        params: params
      }
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    const headers = new HttpHeaders({'Content-Type': 'application/json; charset=utf-8'});

    return this.http.post(
      `${environment.api_url}${path}`,
      JSON.stringify(body),
      {headers: headers}
    ).pipe(catchError(this.formatErrors));
  }

  delete(path, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.delete(
      `${environment.api_url}${path}`, {params})
      .pipe(catchError(this.formatErrors));
  }

}
