import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { Observable } from 'rxjs/Observable';
// import { BehaviorSubject } from 'rxjs/BehaviorSubject';
// import { ReplaySubject } from 'rxjs/ReplaySubject';

import { ApiService } from './api.service';
import { JwtService } from './jwt.service';
// import { map } from 'rxjs/operators/map';
// import { distinctUntilChanged } from 'rxjs/operators/distinctUntilChanged';
// import {Alcoholic} from '../models';


@Injectable()
export class UserService {
  // private currentUserSubject = new BehaviorSubject<Alcoholic>({} as Alcoholic);
  // public currentUser = this.currentUserSubject.asObservable().pipe(distinctUntilChanged());
  //
  // private isAuthenticatedSubject = new ReplaySubject<boolean>(1);
  // public isAuthenticated = this.isAuthenticatedSubject.asObservable();

  constructor (
    private apiService: ApiService,
    private http: HttpClient,
    private jwtService: JwtService
  ) {}

  // populate() {
  //   if (this.jwtService.getToken()) {
  //     this.apiService.get('/user')
  //     .subscribe(
  //       data => this.setAuth(data.user),
  //       err => this.purgeAuth()
  //     );
  //   } else {
  //     this.purgeAuth();
  //   }
  // }
  //
  // setAuth(user: Alcoholic) {
  //   // Save JWT sent from server in localstorage
  //   this.jwtService.saveToken(user.token);
  //   // Set current user data into observable
  //   this.currentUserSubject.next(user);
  //   // Set isAuthenticated to true
  //   this.isAuthenticatedSubject.next(true);
  // }
  //
  // purgeAuth() {
  //   // Remove JWT from localstorage
  //   this.jwtService.destroyToken();
  //   // Set current user to an empty object
  //   this.currentUserSubject.next({} as Alcoholic);
  //   // Set auth status to false
  //   this.isAuthenticatedSubject.next(false);
  // }
  //
  // attemptAuth(type, credentials): Observable<Alcoholic> {
  //   const route = (type === 'login') ? '/login' : '';
  //   return this.apiService.post('/users' + route, {user: credentials})
  //     .pipe(map(
  //     data => {
  //       this.setAuth(data.user);
  //       return data;
  //     }
  //   ));
  // }
  //
  // getCurrentUser(): Alcoholic {
  //   return this.currentUserSubject.value;
  // }
  //
  // // Update the user on the server (email, pass, etc)
  // update(user): Observable<Alcoholic> {
  //   return this.apiService
  //   .put('/user', { user })
  //   .pipe(map(data => {
  //     // Update the currentUser observable
  //     this.currentUserSubject.next(data.user);
  //     return data.user;
  //   }));
  // }

}
