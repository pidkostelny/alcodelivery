import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {PageResponse, Pagination, Type} from '../models';
import {Observable, pipe} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Injectable()
export class CountryService {

  public static URL = '/country';

  constructor(private api: ApiService) {}

  getAll(): Observable<PageResponse> {
    return this.api.get(CountryService.URL);
  }

  getPage(pagination: Pagination = new Pagination()): Observable<PageResponse> {
    return this.api.post(`${CountryService.URL}/page`, pagination);
  }

  delete(id: number): Observable<any> {
    return this.api.delete(CountryService.URL, new HttpParams().set('id', id.toString()));
  }

  create(type: any) {
    return this.api.post(CountryService.URL, type);
  }

  update(type: Type) {
    return this.api.put(CountryService.URL, type, new HttpParams().set('id', type.id.toString()));
  }

  findOneByName(name: string) {
    return this.api.get(`${CountryService.URL}/search`, new HttpParams().set('name', `%${name}%`));
  }
}
