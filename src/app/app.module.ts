import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { CoreModule } from './core/core.module';
import {
  SharedModule,
  NavComponent,
  FooterComponent,
  OrderComponent,
  CartComponent
} from './shared';
import { AuthComponent } from './auth/auth.component';

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    HomeModule,
    AppRoutingModule
  ],
  declarations: [AppComponent, NavComponent, FooterComponent, CartComponent, OrderComponent, AuthComponent],
  entryComponents: [OrderComponent, AuthComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
