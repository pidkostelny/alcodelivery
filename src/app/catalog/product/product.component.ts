import {Component, Input, OnInit} from '@angular/core';
import {CartService, Product} from '../../core';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  productProperties = [
    {name: 'firmName', viewName: 'Виробник', suffix: ''},
    {name: 'maturity', viewName: 'Витримка', suffix: ' р.'},
    {name: 'strength', viewName: 'Міцність', suffix: ' %'},
    {name: 'description', viewName: 'Опис', suffix: ''}
  ];

  @Input() product: Product;
  infoHidden = true;

  constructor(private cartService: CartService) { }

  ngOnInit() {
  }

  addToCart(product: Product) {
    this.cartService.addProductToCart(product);
  }

}
