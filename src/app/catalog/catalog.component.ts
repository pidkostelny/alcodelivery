import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {
  CommodityFilter,
  CommoditySearch,
  CommodityService,
  Country,
  CountryService,
  Firm,
  FirmService,
  Pagination,
  Product, Sort
} from '../core';

const NAV_HEIGHT = 66;
const FIRST_PAGE = 0;

@Component({
  selector: 'app-catalog',
  templateUrl: './catalog.component.html',
  styleUrls: ['./catalog.component.scss']
})
export class CatalogComponent implements OnInit {
  filter = CommodityFilter;

  products: Product[] = [];
  firms: Firm[] = [];
  countries: Country[] = [];

  search: CommoditySearch = new CommoditySearch();

  filtersHidden: boolean;
  filterForm: FormGroup;

  constructor(private fb: FormBuilder,
              private countryService: CountryService,
              private firmService: FirmService,
              private commodityService: CommodityService) {
    this.filterForm = this.fb.group(new CommodityFilter());
    this.filtersHidden = this.screenWidth < 1024;
  }

  ngOnInit() {
    this.commodityService.getPage(this.search.paginationRequest).subscribe(response => {
      this.setProductDataFromApi(response);
    });
    this.firmService.getPage(new Pagination()).subscribe(response => {
      this.firms = response.content;
    });
    this.countryService.getPage(new Pagination()).subscribe(response => {
      this.countries = response.content;
    });
  }

  onResetForm() {
    this.filterForm.reset(new CommodityFilter());
    this.filterForm.markAsUntouched();
    this.loadData(FIRST_PAGE);
  }

  onPageChange(page: number) {
    this.loadData(page);
  }

  onFirmClick(checked: boolean, val: Firm) {
    this.filterForm.markAsTouched();
    const firmsId = this.search.firmsId;
    checked ? firmsId.push(val.id) : firmsId.splice(firmsId.indexOf(val.id), 1);
    this.loadData(FIRST_PAGE);
  }

  onCountryClick(checked: boolean, val: Country) {
    this.filterForm.markAsTouched();
    const countriesId = this.search.countriesId;
    checked ? countriesId.push(val.id) : countriesId.splice(countriesId.indexOf(val.id), 1);
    this.loadData(FIRST_PAGE);
  }

  onFiltersClick() {
    this.filtersHidden = !this.filtersHidden;
  }

  onSortDirectionChange(sort: Sort) {
    this.search.paginationRequest.sort.direction = sort.direction;
    this.search.paginationRequest.sort.fieldName = sort.fieldName;
    this.loadData(FIRST_PAGE);
  }

  onFilterChange() {
    this.filterForm.markAsTouched();
    this.search.maxPrice = this.maxPrice.value;
    this.search.minPrice = this.minPrice.value;
    this.search.minVolume = this.minVolume.value * 1000;
    this.search.maxVolume = this.maxVolume.value * 1000;
    this.loadData(FIRST_PAGE);
  }

  loadData(page?: number) {
    if (page) this.search.paginationRequest.page = page;
    if (this.filterForm.touched) {
      this.commodityService.search(this.search).subscribe(response => {
        this.setProductDataFromApi(response);
      });
    } else {
      this.commodityService.getPage(this.search.paginationRequest).subscribe(response => {
        this.setProductDataFromApi(response);
      });
    }
  }

  setProductDataFromApi(data) {
    this.products = data.content;
    this.search.paginationRequest.total = data.pagination.total;
  }

  hideFilters() {
    this.filtersHidden = true;
  }

  showFilters() {
    this.filtersHidden = false;
  }

  get screenWidth() {
    return window.innerWidth;
  }

  get minPrice() {
    return this.filterForm.get('minPrice');
  }

  get maxPrice() {
    return this.filterForm.get('maxPrice');
  }

  get minVolume() {
    return this.filterForm.get('minVolume');
  }

  get maxVolume() {
    return this.filterForm.get('maxVolume');
  }
}
