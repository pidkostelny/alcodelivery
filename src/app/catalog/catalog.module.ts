
import {CatalogRoutingModule} from './catalog-routing.module';
import {CatalogComponent} from './catalog.component';
import {NgModule} from '@angular/core';
import {SharedModule} from '../shared';
import {ProductComponent} from './product/product.component';
import {PaginationComponent} from './pagination/pagination.component';

@NgModule({
  imports: [
    CatalogRoutingModule,
    SharedModule
  ],
  declarations: [
    CatalogComponent,
    ProductComponent,
    PaginationComponent
  ],
  providers: [

  ]
})
export class CatalogModule {}
