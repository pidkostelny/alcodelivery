import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {Pagination} from '../../core/models';

const PAGES_TO_SHOW = 5;

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnChanges {
  @Input() pagination: Pagination;

  @Output() pageChange = new EventEmitter<number>();
  pagesCount;
  pagesToShow = [];

  ngOnChanges(changes: SimpleChanges) {
    this.initPagination();
  }

  onPageClick(page) {
    // if (page !== this.pagination.page - 1) {
      this.pageChange.emit(page);
    // }
  }

  initPagination() {
    this.pagesCount = Math.ceil(this.pagination.total / this.pagination.size) + 2;
    let page;
    if (this.pagination.page < 3) {
      page = 1;
    } else if (this.pagination.total > 4) {
      page = this.pagination.page - 1;
    }
    for (let count = 0; page < this.pagesCount && count < PAGES_TO_SHOW; page++, count++) {
      this.pagesToShow.push({number: page, selected: page === this.pagination.page + 1});
    }
  }
}
